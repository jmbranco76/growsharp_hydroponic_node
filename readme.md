# Grow# #

##Grow Sharp Hydroponic Node.

This repository has the EFM32WG files for the hydroponic node. This node is based on a EFM32WG board from SiliconLabs and has all the files included in the SimplicityStudio project.
This node handles the communication between the base controller and the base node via ez radio [see here](https://bitbucket.org/jmbranco76/growsharp_base_controller). It also communicates via USART with the MPPT board [see here](https://bitbucket.org/jmbranco76/growsharp_mppt_code).

Board:  Silicon Labs SLWSTK6220A_EZR32WG Development Kit
Device: EZR32WG330F256R60