/*
 * sensors.h
 *
 *  Created on: 25 de Out de 2015
 *      Author: Jos�
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#include "stuff.h"
#include "datastructures.h"


#define DMA_CHANNEL    	4
#define NUM_SAMPLES    	6
#define ADC_PH_CH		0
#define ADC_EC_CH		1
#define ADC_NTC1_CH		2
#define ADC_NTC2_CH		3
#define ADC_NTC3_CH		4
#define ADC_NTC4_CH		5

void sensorsSpinFunction();
uint32_t readSensors(void);
void DMAConfig(void);
uint32_t *getADCsPtr();
float getTemp(uint32_t adc);

#endif /* SENSORS_H_ */
