/*
 * aes.c
 *
 *  Created on: 28 de Out de 2015
 *      Author: JMBM
 */

#include "aes.h"

unsigned char key1[]   = {'0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

uint8_t encryptData(uint8_t *data, uint8_t size)
{
	uint8_t temp_key[16];

	int s = size/16;

	int i;
	for (i = 0; i < s; ++i) {
		memcpy(temp_key, key1, 16);
		aes_enc_dec(data+(16*i), temp_key, 0);
	}
}

uint8_t decryptData(uint8_t *data, uint8_t size)
{
	uint8_t temp_key[16];

	uint8_t s = size/16;

	int i;
	for (i = 0; i < s; ++i) {
		memcpy(temp_key, key1, 16);
		aes_enc_dec(data+(16*i), temp_key, 1);
	}
}
