/*
 * board_io.h
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#ifndef BOARD_IO_H_
#define BOARD_IO_H_

#include "stuff.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PWM_FREQ				200000

#define PWM1_PORT				gpioPortE
#define PWM1_PIN				0
#define PWM2_PORT				gpioPortE
#define PWM2_PIN				1
#define PWM3_PORT				gpioPortE
#define PWM3_PIN				2
#define PWM4_PORT				gpioPortF
#define PWM4_PIN				7

#define SENSOR_ENABLE_PORT		gpioPortF
#define SENSOR_ENABLE_PIN		8

#define I2C_periph				I2C0

void initIO(void);
void deployValve(uint8_t state);
void nutrientValve(uint8_t state, uint8_t id);
uint8_t getHighLevelSensor();
uint8_t getLowLevelSensor();
void initPWMs();
uint8_t setPWMs(uint8_t index, uint32_t value);

#ifdef __cplusplus
}
#endif

#endif /* BOARD_IO_H_ */
