/*
 * growing_lights.cpp
 *
 *  Created on: 8 de Nov de 2015
 *      Author: Jo�o Sousa
 */


#include "growing_lights.h"

void lightsSpinFunction()
{
	float x,x0,x1,y1,y0;
	DateTime d;
	convertUnixTimeToDate(getUnixTimestamp(), &d);
	x = (float)d.hours + (float)d.minutes/60.0;
	uint8_t i, f;
	getIndexHours(d.hours, &i, &f);

	for (int i_led = 0; i_led < 4; ++i_led) {

		x1 = (float)getControlMessagePtr()->day_light_hours[f];
		x0 = (float)getControlMessagePtr()->day_light_hours[i];
		y1 = getControlMessagePtr()->day_light_pwm[f][i_led];
		y0 = getControlMessagePtr()->day_light_pwm[i][i_led];
		float y = (x - x0)/(x1-x0)*(y1-y0)+y0;
		if(getControlMessagePtr()->force_pwm[i_led])
		{
			setPWMs(i_led+1, 255-(uint8_t)y);
		}
	}
}

void getIndexHours(uint8_t h, uint8_t *i, uint8_t *f)
{
	bool found = false;
	for (int ind = 0; ((ind < NUMBER_INDEX_HOURS-1) && !found); ++ind)
	{
		if(h >= getControlMessagePtr()->day_light_hours[ind] && h < getControlMessagePtr()->day_light_hours[ind+1])
		{
			found = true;
			*i = ind;
			*f = ind + 1;
		}
	}

	if(!found)
	{
		*i = NUMBER_INDEX_HOURS-1;
		*f = 0;
	}
}
