/**
 * @file date_time.c
 * @brief Date and time management
 *
 * @section License
 *
 * Copyright (C) 2010-2015 Oryx Embedded SARL. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @author Oryx Embedded SARL (www.oryx-embedded.com)
 * @version 1.6.0
 **/

//Dependencies
#include <stdio.h>
#include <string.h>
#include "date_time.h"

#if defined(_WIN32)
#include <time.h>
#endif

//Days
static const char days[8][10] =
{
		"",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		"Sunday"
};

//Months
static const char months[13][10] =
{
		"",
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
};


/**
 * @brief Convert Unix timestamp to date
 * @param[in] t Unix timestamp
 * @param[out] date Pointer to a structure representing the date and time
 **/

void convertUnixTimeToDate(time_t t, DateTime *date)
{
	uint32_t a;
	uint32_t b;
	uint32_t c;
	uint32_t d;
	uint32_t e;
	uint32_t f;

	//Negative Unix time values are not supported
	if(t < 1) t = 0;

	//Clear milliseconds
	date->milliseconds = 0;

	//Retrieve hours, minutes and seconds
	date->seconds = t % 60;
	t /= 60;
	date->minutes = t % 60;
	t /= 60;
	date->hours = t % 24;
	t /= 24;

	//Convert Unix time to date
	a = (uint32_t) ((4 * t + 102032) / 146097 + 15);
	b = (uint32_t) (t + 2442113 + a - (a / 4));
	c = (20 * b - 2442) / 7305;
	d = b - 365 * c - (c / 4);
	e = d * 1000 / 30601;
	f = d - e * 30 - e * 601 / 1000;

	//January and February are counted as months 13 and 14 of the previous year
	if(e <= 13)
	{
		c -= 4716;
		e -= 1;
	}
	else
	{
		c -= 4715;
		e -= 13;
	}


	//Retrieve year, month and day
	date->year = c;
	date->month = e;
	date->day = f;


	// DLS
	uint8_t dsl = 0;
	if(date->month > 3 && date->month < 10)
	{
		dsl = 1;
	}
	else if(date->month == 3)
	{
		// get last sunday
		uint8_t last_sunday = getLastSunday(date->year, date->month);
		if(date->day > last_sunday)
		{
			dsl = 1;
		}
		else if(date->day == last_sunday)
		{
			if(date->hours >= 1)
			{
				dsl = 1;
			}
		}
	}
	else if(date->month == 10)
	{
		// get last sunday
		uint8_t last_sunday = getLastSunday(date->year, date->month);
		if(date->day < last_sunday)
		{
			dsl = 1;
		}
		else if(date->day == last_sunday)
		{
			if(date->hours < 1)
			{
				dsl = 1;
			}
		}
	}

	date->hours += dsl;

	if(date->hours >= 24)
	{
		date->hours = 0;
		date->day++;

		switch (date->month) {
		case 2:
			if(isLeapYear(date->year))
			{
				if(date->day>29)
				{
					date->day = 1;
					date->month++;
				}
			}
			else
			{
				if(date->day>28)
				{
					date->day = 1;
					date->month++;
				}
			}
			break;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if(date->day>31)
			{
				date->day = 1;
				date->month++;
			}
			break;
		default:
			if(date->day>30)
			{
				date->day = 1;
				date->month++;
			}
			break;
		}
		if(date->month >12)
		{
			date->month = 1;
			date->year++;
		}
	}

	//Calculate day of week
	date->dayOfWeek = computeDayOfWeek(c, e, f);
}


/**
 * @brief Convert date to Unix timestamp
 * @param[in] date Pointer to a structure representing the date and time
 * @return Unix timestamp
 **/

time_t convertDateToUnixTime(const DateTime *date)
{
	uint32_t y;
	uint32_t m;
	uint32_t d;
	uint32_t t;

	//Year
	y = date->year;
	//Month of year
	m = date->month;
	//Day of month
	d = date->day;

	//January and February are counted as months 13 and 14 of the previous year
	if(m <= 2)
	{
		m += 12;
		y -= 1;
	}

	//Convert years to days
	t = (365 * y) + (y / 4) - (y / 100) + (y / 400);
	//Convert months to days
	t += (30 * m) + (3 * (m + 1) / 5) + d;
	//Unix time starts on January 1st, 1970
	t -= 719561;
	//Convert days to seconds
	t *= 86400;
	//Add hours, minutes and seconds
	t += (3600 * date->hours) + (60 * date->minutes) + date->seconds;

	//Return Unix time
	return t;
}


/**
 * @brief Calculate day of week
 * @param[in] y Year
 * @param[in] m Month of year (in range 1 to 12)
 * @param[in] d Day of month (in range 1 to 31)
 * @return Day of week (in range 1 to 7)
 **/

uint8_t computeDayOfWeek(uint16_t y, uint8_t m, uint8_t d)
{
	uint32_t h;
	uint32_t j;
	uint32_t k;

	//January and February are counted as months 13 and 14 of the previous year
	if(m <= 2)
	{
		m += 12;
		y -= 1;
	}

	//J is the century
	j = y / 100;
	//K the year of the century
	k = y % 100;

	//Compute H using Zeller's congruence
	h = d + (26 * (m + 1) / 10) + k + (k / 4) + (5 * j) + (j / 4);

	//Return the day of the week
	return ((h + 5) % 7) + 1;
}

uint8_t getLastSunday(uint16_t y, uint8_t month)
{
	uint8_t day = 31, dow;
	while(1)//Sunday
	{
		dow = computeDayOfWeek(y, month, day);
		if(dow == 7)
		{
			break;
		}
		else
		{
			day--;
		}
	}
	return day;
}


uint8_t isLeapYear(uint16_t year)
{
	return (((year - 2012)%4) == 0) ? 1 : 0;
}
