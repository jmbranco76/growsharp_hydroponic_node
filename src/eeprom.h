/*
 * eeprom.h
 *
 *  Created on: 19 de Out de 2015
 *      Author: Jos�
 */

#ifndef EEPROM_H_
#define EEPROM_H_

#include "stuff.h"

#define EEPROM_ADDR		0xA0

int readCalibration(I2C_TypeDef *i2c, uint8_t *sensors);
int writeCalibration(I2C_TypeDef *i2c, uint8_t *sensors);


#endif /* EEPROM_H_ */
