/*
 * radio.c
 *
 *  Created on: 24 de Out de 2015
 *      Author: Jos�
 */

#include "radio.h"

/* RTC callback parameters. */
static void (*rtcCallback)(void*) = NULL;
static void * rtcCallbackArg = 0;

// RADIO CB
#if (defined EZRADIO_VARIABLE_DATA_START)
#define APP_PKT_DATA_START EZRADIO_VARIABLE_DATA_START
#else
#define APP_PKT_DATA_START 1u
#endif

#if ( defined EZRADIO_PLUGIN_TRANSMIT )
void appPacketTransmittedCallback ( EZRADIODRV_Handle_t handle, Ecode_t status );
#endif //#if ( defined EZRADIO_PLUGIN_TRANSMIT )

#if ( defined EZRADIO_PLUGIN_RECEIVE )
void appPacketReceivedCallback ( EZRADIODRV_Handle_t handle, Ecode_t status );
#endif //#if ( defined EZRADIO_PLUGIN_RECEIVE )

#if ( defined EZRADIO_PLUGIN_CRC_ERROR )
void appPacketCrcErrorCallback ( EZRADIODRV_Handle_t handle, Ecode_t status );
#endif //#if ( defined EZRADIO_PLUGIN_CRC_ERROR )


#if ( defined EZRADIO_PLUGIN_TRANSMIT )

/* Defines the number of packets to send for one press of PB1.
 * Sends infinite number of packets if defined to 0xFFFF. */
#define APP_TX_PKT_SEND_NUM   0xFFFF

/* Tx packet data array, initialized with the default payload in the generated header file */
uint8_t radioTxPkt[EZRADIO_FIFO_SIZE] = RADIO_CONFIG_DATA_CUSTOM_PAYLOAD;

/* Packet counter */
uint16_t appTxPktCntr = 0;

/* Sign tx active state */
bool     appTxActive = false;

/* Data counter in transmitted packet */
uint16_t appDataCntr = 0;

#endif //#if ( defined EZRADIO_PLUGIN_TRANSMIT )

#if ( defined EZRADIO_PLUGIN_RECEIVE )

/* Rx packet data array */
uint8_t radioRxPkt[EZRADIO_FIFO_SIZE];

/* Received data */
uint16_t appRxData;

#endif //#if ( defined EZRADIO_PLUGIN_RECEIVE )

/* RTC frequency */
#define APP_RTC_FREQ_HZ 200u
/* RTC timeout */
#define APP_RTC_TIMEOUT_MS ( 1000u / APP_RTC_FREQ_HZ )

/* RTC set time is expired */
bool rtcTick = false;

/** Timer used to issue time elapsed interrupt. */
static RTCDRV_TimerID_t rtcTickTimer;
static RTCDRV_TimerID_t rtcRepeateTimer;

/* EZRadio driver init data and handler */
EZRADIODRV_HandleData_t appRadioInitData = EZRADIODRV_INIT_DEFAULT;
EZRADIODRV_Handle_t appRadioHandle = &appRadioInitData;

/* =========================================================================
 * =========================================================================
 * =========================================================================
 * =========================================================================
 * =========================================================================
 */
Control_Node ctrl_msg;


uint8_t sendNutrientMessage(HydroponicNode_type *ctrl_msg)
{
#if ( defined EZRADIO_PLUGIN_TRANSMIT )
	/* Try to send the packet */
	if ( !appTxActive )
	{
		/* Sing tx active state */
		appTxActive = true;

		// copy message
		memcpy(radioTxPkt+1,ctrl_msg, sizeof(HydroponicNode_type));
		radioTxPkt[0] = HidroponicMessage;
		encryptData(radioTxPkt, 32);

		/* Transmit packet */
		ezradioStartTransmitDefault( appRadioHandle, radioTxPkt );
	}

#endif //#if ( defined EZRADIO_PLUGIN_TRANSMIT )
}

uint8_t sendControlMessage(Control_Node *ctrl_msg)
{
	uint8_t encript[32];
#if ( defined EZRADIO_PLUGIN_TRANSMIT )
	/* Try to send the packet */
	if ( !appTxActive )
	{
		/* Sing tx active state */
		appTxActive = true;

		// copy message
		memcpy(radioTxPkt+1,ctrl_msg, sizeof(Control_Node));
		radioTxPkt[0] = ControlMessage;

		memcpy(encript, radioTxPkt, 32);
		encryptData(encript, 32);

		/* Transmit packet */
		ezradioStartTransmitDefault( appRadioHandle, encript );
	}

#endif //#if ( defined EZRADIO_PLUGIN_TRANSMIT )
}

uint8_t sendPhotovoltaicMessage(Photovoltaic_type *msg)
{
#if ( defined EZRADIO_PLUGIN_TRANSMIT )
	/* Try to send the packet */
	if ( !appTxActive )
	{
		/* Sing tx active state */
		appTxActive = true;

		// copy message
		memcpy(radioTxPkt+1,msg, sizeof(Photovoltaic_type));
		radioTxPkt[0] = PhotovoltaicMessage;
		encryptData(radioTxPkt, 32);

		/* Transmit packet */
		ezradioStartTransmitDefault( appRadioHandle, radioTxPkt );
	}

#endif //#if ( defined EZRADIO_PLUGIN_TRANSMIT )
}






/*=========================================================================
 * =========================================================================
 * =========================================================================
 * =========================================================================
 * =========================================================================
 * =========================================================================
 */

void init_radios()
{
	/* EZRadio response structure union */
	ezradio_cmd_reply_t ezradioReply;

	RTCDRV_Init();
	if (ECODE_EMDRV_RTCDRV_OK !=
			RTCDRV_AllocateTimer( &rtcTickTimer) )
	{
		while (1);
	}
	if (ECODE_EMDRV_RTCDRV_OK !=
			RTCDRV_StartTimer(rtcTickTimer, rtcdrvTimerTypePeriodic, APP_RTC_TIMEOUT_MS,
					(RTCDRV_Callback_t)RTC_App_IRQHandler, NULL ) )
	{
		while (1);
	}

#if ( defined EZRADIO_PLUGIN_TRANSMIT )
	/* Configure packet transmitted callback. */
	appRadioInitData.packetTx.userCallback = &appPacketTransmittedCallback;
#endif

#if ( defined EZRADIO_PLUGIN_RECEIVE )
	/* Configure packet received buffer and callback. */
	appRadioInitData.packetRx.userCallback = &appPacketReceivedCallback;
	appRadioInitData.packetRx.pktBuf = radioRxPkt;
#endif

#if ( defined EZRADIO_PLUGIN_CRC_ERROR )
	/* Configure packet received with CRC error callback. */
	appRadioInitData.packetCrcError.userCallback = &appPacketCrcErrorCallback;
#endif

	/* Initialize EZRadio device. */
	ezradioInit( appRadioHandle );

	/* Reset radio fifos and start reception. */
	ezradioResetTRxFifo();
#if ( defined EZRADIO_PLUGIN_RECEIVE )
	ezradioStartRx( appRadioHandle );
#endif

}


/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB0)
 *        Increments the time by one minute.
 *****************************************************************************/
void RTC_App_IRQHandler()
{
	rtcTick = true;
	addTime();
}


/**************************************************************************//**
 * @brief   The actual callback for Memory LCD toggling
 * @param[in] id
 *   The id of the RTC timer (not used)
 *****************************************************************************/
static void memLcdCallback(RTCDRV_TimerID_t id, void *user)
{
	(void) id;
	(void) user;
	rtcCallback(rtcCallbackArg);
}


/**************************************************************************//**
 * @brief   Register a callback function to be called repeatedly at the
 *          specified frequency.
 *
 * @param[in] pFunction  Pointer to function that should be called at the
 *                       given frequency.
 * @param[in] pParameter Pointer argument to be passed to the callback function.
 * @param[in] frequency  Frequency at which to call function at.
 *
 * @return  0 for successful or
 *         -1 if the requested frequency is not supported.
 *****************************************************************************/
int RepeatCallbackRegister (void(*pFunction)(void*),
		void* pParameter,
		unsigned int frequency)
{

	if (ECODE_EMDRV_RTCDRV_OK ==
			RTCDRV_AllocateTimer( &rtcRepeateTimer))
	{
		if (ECODE_EMDRV_RTCDRV_OK ==
				RTCDRV_StartTimer(rtcRepeateTimer, rtcdrvTimerTypePeriodic, frequency,
						(RTCDRV_Callback_t)pFunction, pParameter ))
		{
			return 0;
		}
	}

	return -1;
}

/**************************************************************************//**
 * @brief   Register a callback function at the given frequency.
 *
 * @param[in] pFunction  Pointer to function that should be called at the
 *                       given frequency.
 * @param[in] argument   Argument to be given to the function.
 * @param[in] frequency  Frequency at which to call function at.
 *
 * @return  0 for successful or
 *         -1 if the requested frequency does not match the RTC frequency.
 *****************************************************************************/
int rtcIntCallbackRegister(void (*pFunction)(void*),
		void* argument,
		unsigned int frequency)
{
	RTCDRV_TimerID_t timerId;
	rtcCallback    = pFunction;
	rtcCallbackArg = argument;

	RTCDRV_AllocateTimer(&timerId);

	RTCDRV_StartTimer(timerId, rtcdrvTimerTypePeriodic, 1000 / frequency,
			memLcdCallback, NULL);


	return 0;
}

#if ( defined EZRADIO_PLUGIN_TRANSMIT )
/**************************************************************************//**
 * @brief  Packet transmitted callback of the application.
 *
 * @param[in] handle EzRadio plugin manager handler.
 * @param[in] status Callback status.
 *****************************************************************************/
void appPacketTransmittedCallback ( EZRADIODRV_Handle_t handle, Ecode_t status )
{
	if ( status == ECODE_EMDRV_EZRADIODRV_OK )
	{
		/* Sign tx passive state */
		appTxActive = false;

#if ( defined EZRADIO_PLUGIN_RECEIVE )
		/* Change to RX state */
		ezradioStartRx( handle );
#endif
	}
}
#endif //#if ( defined EZRADIO_PLUGIN_TRANSMIT )

#if ( defined EZRADIO_PLUGIN_RECEIVE )
/**************************************************************************//**
 * @brief  Packet received callback of the application.
 *
 * @param[in] handle EzRadio plugin manager handler.
 * @param[in] status Callback status.
 *****************************************************************************/
void appPacketReceivedCallback ( EZRADIODRV_Handle_t handle, Ecode_t status )
{
	if ( status == ECODE_EMDRV_EZRADIODRV_OK )
	{
		/* Read out and print received packet data:
		 *  - print 'ACK' in case of ACK was received
		 *  - print the data if some other data was received. */
		if ( (radioRxPkt[APP_PKT_DATA_START] == 'A') &&
				(radioRxPkt[APP_PKT_DATA_START + 1] == 'C') &&
				(radioRxPkt[APP_PKT_DATA_START + 2] == 'K') )
		{
			printf("-->Data RX: ACK\n");
		}
		else
		{
			decryptData( radioRxPkt, 32);
			switch (radioRxPkt[0]) {
				case ControlMessage:
					memcpy(getControlMessagePtr(), radioRxPkt+1, sizeof(Control_Node));
					newControlMessage();
					break;
				default:
					break;
			}

		}
	}
}
#endif //#if ( defined EZRADIO_PLUGIN_RECEIVE )

#if ( defined EZRADIO_PLUGIN_CRC_ERROR )
/**************************************************************************//**
 * @brief  Packet received with CRC error callback of the application.
 *
 * @param[in] handle EzRadio plugin manager handler.
 * @param[in] status Callback status.
 *****************************************************************************/
void appPacketCrcErrorCallback ( EZRADIODRV_Handle_t handle, Ecode_t status )
{
	if ( status == ECODE_EMDRV_EZRADIODRV_OK )
	{
		printf("-->Pkt  RX: CRC Error\n");

#if ( defined EZRADIO_PLUGIN_RECEIVE )
		/* Change to RX state */
		ezradioStartRx( handle );
#endif //#if ( defined EZRADIO_PLUGIN_RECEIVE )
	}
}
#endif //#if ( defined EZRADIO_PLUGIN_CRC_ERROR )
