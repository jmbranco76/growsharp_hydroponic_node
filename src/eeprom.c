/*
 * eeprom.c
 *
 *  Created on: 19 de Out de 2015
 *      Author: Jos�
 */


#include "eeprom.h"

int readCalibration(I2C_TypeDef *i2c, uint8_t *sensors)
{
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_write_data[2];
	uint8_t                    i2c_read_data[32];
	seq.addr  = EEPROM_ADDR;
	seq.flags = I2C_FLAG_WRITE_READ;
	/* Select command to issue */
	i2c_write_data[0] = 0x00;
	i2c_write_data[1] = 0x00;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 2;
	/* Select location/length of data to be read */
	seq.buf[1].data = sensors;
	seq.buf[1].len  = 32;

	ret = I2CSPM_Transfer(i2c, &seq);

	if (ret != i2cTransferDone)
	{
		return((int) ret);
	}
	return 32;
}

int writeCalibration(I2C_TypeDef *i2c, uint8_t *sensors)
{
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_write_data[34];
	uint8_t                    i2c_read_data[2];

	seq.addr  = EEPROM_ADDR;
	seq.flags = I2C_FLAG_WRITE;
	/* Select command to issue */
	i2c_write_data[0] = 0x00;
	i2c_write_data[1] = 0x00;
	memcpy(i2c_write_data+2,(uint8_t *)sensors, 32);
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 34;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 0;

	ret = I2CSPM_Transfer(i2c, &seq);

	if (ret != i2cTransferDone)
	{
		return((int) ret);
	}
	return 32;
}
