/*
 * time_board.cpp
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#include "time_board.h"

uint32_t time__ = 0;
uint32_t unix_timestamp = 0;

void addTime()
{
	time__+=5;
	if((time__%1000) == 0)
	{
		unix_timestamp+=1;
	}
}

uint32_t getTime()
{
	return time__;
}

uint32_t setUnixTimestamp(uint32_t t)
{
	unix_timestamp = t;
	return unix_timestamp;
}

uint32_t getUnixTimestamp()
{
	return unix_timestamp;
}

uint32_t getTimeSeconds()
{
	return time__/1000;
}
