/**************************************************************************//**
 * @file
 * @brief USART/LEUART RS232 example for SLWSTK6220A_EZR32WG starter kit
 * @version 4.0.0
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include "stuff.h"


#include <stdint.h>

/*
 * Note! You can set compile time define -DRETARGET_LEUART0 to build this
 * example to use LEUART0 instead of default USART 0 or 1.
 * See retargetserialconfig.h for details.
 */

#if !defined(__CROSSWORKS_ARM) && defined(__GNUC__)
/* iprintf does not process floats, but occupy less flash memory ! */
#define printf iprintf
#endif

/** Time (in ms) between periodic updates of the measurements. */
#define PERIODIC_UPDATE_MS      5
/** RS232 input buffer size */
#define ECHOBUFSIZE    80
/** RS232 Input buffer */

/** Timer used for periodic update of the measurements. */
RTCDRV_TimerID_t periodicUpdateTimerId;
/** This flag tracks if we need to perform a new
 *  measurement. */

/**************************************************************************//**
 * Local variables
 *****************************************************************************/
/* RTC callback parameters. */

static void adcInit(void);
static void gpioSetup(void);
void initI2C(void);


I2CSPM_Init_TypeDef i2cInit = { I2C0,                      /* Use I2C instance 0 */                        \

		gpioPortD,                 /* SCL port */                                  \
		7,                         /* SCL pin */                                   \
		gpioPortD,                 /* SDA port */                                  \
		6,                         /* SDA pin */                                   \
		1,                         /* Location */                                  \
		0,                         /* Use currently configured reference clock */  \
		I2C_FREQ_STANDARD_MAX,     /* Set to standard rate  */                     \
		i2cClockHLRStandard,       /* Set to use 4:4 low/high duty cycle */        \
};

extern EZRADIODRV_Handle_t appRadioHandle;

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
	/* Chip errata */
	CHIP_Init();



	/* Initalize hardware */
	gpioSetup();
	initI2C();
	initComm(i2cInit.port);
	init_radios();
	adcInit();

	/* Initialize LEUART/USART and map LF to CRLF */
	RETARGET_SerialInit();
	RETARGET_SerialCrLf(1);

#if defined(RETARGET_LEUART0)
	printf("\nEZR32 LEUART example\n");
#elif defined(RETARGET_VCOM)
	printf("\nEZR32 VCOM example\n");
#else
	printf("\nEZR32 USART example\n");
#endif

	printf("clock %d\n",CMU_ClockFreqGet(cmuClock_CORE));

	uint32_t last = getTime();



	setUnixTimestamp(1420106400);
	getControlMessagePtr()->day_light_hours[0] = 0;
	getControlMessagePtr()->day_light_hours[1] = 7;
	getControlMessagePtr()->day_light_hours[2] = 11;
	getControlMessagePtr()->day_light_hours[3] = 16;

	getControlMessagePtr()->day_light_pwm[0][0] = 0;
	getControlMessagePtr()->day_light_pwm[0][1] = 0;
	getControlMessagePtr()->day_light_pwm[0][2] = 0;
	getControlMessagePtr()->day_light_pwm[0][3] = 0;

	getControlMessagePtr()->day_light_pwm[1][0] = 0;
	getControlMessagePtr()->day_light_pwm[1][1] = 0;
	getControlMessagePtr()->day_light_pwm[1][2] = 0;
	getControlMessagePtr()->day_light_pwm[1][3] = 0;

	getControlMessagePtr()->day_light_pwm[2][0] = 255;
	getControlMessagePtr()->day_light_pwm[2][1] = 255;
	getControlMessagePtr()->day_light_pwm[2][2] = 255;
	getControlMessagePtr()->day_light_pwm[2][3] = 0;

	getControlMessagePtr()->day_light_pwm[3][0] = 255;
	getControlMessagePtr()->day_light_pwm[3][1] = 255;
	getControlMessagePtr()->day_light_pwm[3][2] = 255;
	getControlMessagePtr()->day_light_pwm[3][3] = 0;

	while (1)
	{
		/* Run radio plug-in manager */
		ezradioPluginManager( appRadioHandle );

		//
		parseCmd();

		sensorsSpinFunction();

		// get new messages
		readNewMessages();

		sendMessages();

		servoSpinFunction();

		if(getTime() - last > 10)
		{
			lightsSpinFunction();

		}

	}
}






/**************************************************************************//**
 * @brief ADC Initialization
 *****************************************************************************/
static void adcInit(void)
{
	ADC_Init_TypeDef       init       = ADC_INIT_DEFAULT;
	//   ADC_InitSingle_TypeDef initSingle = ADC_INITSINGLE_DEFAULT;

	/* Enable ADC clock */
	CMU_ClockEnable( cmuClock_ADC0, true );

	/* Initiate ADC peripheral */
	init.ovsRateSel = adcOvsRateSel32;
	init.lpfMode = adcLPFilterDeCap;
	ADC_Init(ADC0, &init);

	ADC_InitScan_TypeDef scanInit = ADC_INITSCAN_DEFAULT;

	/* Init common issues for both single conversion and scan mode */
	init.timebase = ADC_TimebaseCalc(0);
	init.prescale = ADC_PrescaleCalc(7000000, 0);
	ADC_Init(ADC0, &init);

	/* Init for scan sequence use ( for dvk: accelerometer X, Y and Z axis). */
	scanInit.reference = adcRefVDD;
	scanInit.input     = ADC_SCANCTRL_INPUTMASK_CH0 |
			ADC_SCANCTRL_INPUTMASK_CH1 |
			ADC_SCANCTRL_INPUTMASK_CH2 |
			ADC_SCANCTRL_INPUTMASK_CH3 |
			ADC_SCANCTRL_INPUTMASK_CH4 |
			ADC_SCANCTRL_INPUTMASK_CH5; //|
	//ADC_SCANCTRL_INPUTMASK_CH6 |
	//ADC_SCANCTRL_INPUTMASK_CH7;
	ADC_InitScan(ADC0, &scanInit);

	/* Manually set some calibration values */
	//ADC0->CAL = (0x7C << _ADC_CAL_SINGLEOFFSET_SHIFT) | (0x1F << _ADC_CAL_SINGLEGAIN_SHIFT);

	/* Enable interrupt on completed conversion */
	//	ADC_IntEnable(ADC0, ADC_IF_SCAN);
	//	NVIC_ClearPendingIRQ( ADC0_IRQn );
	//	NVIC_EnableIRQ( ADC0_IRQn );


	DMAConfig();
}

/**************************************************************************//**
 * @brief Setup GPIO interrupt for pushbuttons.
 *****************************************************************************/
static void gpioSetup(void)
{
	/* Enable GPIO clock */
	/* Enable GPIO clock */
	CMU_ClockEnable(cmuClock_GPIO, true);

	/* Initialize GPIO interrupt */
	GPIOINT_Init();

	initIO();
}

void initI2C(void)
{
	I2CSPM_Init(&i2cInit);
}
