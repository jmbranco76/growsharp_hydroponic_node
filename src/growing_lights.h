/*
 * growing_lights.h
 *
 *  Created on: 8 de Nov de 2015
 *      Author: Jo�o Sousa
 */

#ifndef GROWING_LIGHTS_H_
#define GROWING_LIGHTS_H_

#include "stuff.h"
#include "stdint.h"
#ifdef __cplusplus
extern "C"
{
#endif
#include "date_time.h"
#ifdef __cplusplus
}
#endif

#define NUMBER_INDEX_HOURS		4

void lightsSpinFunction();
void getIndexHours(uint8_t h, uint8_t *i, uint8_t *f);


#endif /* GROWING_LIGHTS_H_ */
