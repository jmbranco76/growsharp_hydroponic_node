/*
 * servo.c
 *
 *  Created on: 29 de Out de 2015
 *      Author: JMBM
 */

#include "servo.h"

#define SERVO_MAX 	4225
#define SERVO_MID	2925
#define SERVO_MIN 	1525
#define OFFSET		0
#define COUNTER_TOP	2
#define SERVO_MEASURE_ANGLE	25
#define SERVO_UP_ANGLE		-70
#define SERVO_MEASURE_TIME	1000
#define SERVO_MEASURE_WAIT_TIME	10000

int angulo = 0, curr_angle = 0;
uint8_t counter = 0;
uint32_t last_servo_measure =0;
PH_Servo_State servo_ph_state = Idle_State;

void initServo(void)
{

	CMU_ClockEnable(cmuClock_TIMER2, true);
	GPIO_PinModeSet(SERVO_PORT, SERVO_PIN, gpioModePushPull, 0);

	// Create the timer count control object initializer
	TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
	timerCCInit.mode = timerCCModePWM;
	timerCCInit.cmoa = timerOutputActionToggle;
	// Configure CC channel 0
	TIMER_InitCC(TIMER2, 1, &timerCCInit);

	TIMER2->ROUTE |= (TIMER_ROUTE_CC1PEN | TIMER_ROUTE_LOCATION_LOC1);

	/* Set Top Value */
	TIMER_TopSet(TIMER2, 35000);
	TIMER_CompareBufSet(TIMER2, 1, SERVO_MID);

	TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
	timerInit.prescale = timerPrescale8;

	TIMER_IntEnable(TIMER2, TIMER_IF_OF);
	/* Enable TIMER2 interrupt vector in NVIC */
	NVIC_EnableIRQ(TIMER2_IRQn);

	/* Enable overflow interrupt */
	TIMER_Init(TIMER2, &timerInit);

	setServo(-SERVO_MEASURE_ANGLE);
	curr_angle = SERVO_UP_ANGLE;
}

uint32_t map_servo(int angle)
{
	//	angle += OFFSET;
	float a;
	if(angle == 0)
	{
		a = SERVO_MID;
	}
	else if (angle < 0 && angle >= -80)
	{
		a = SERVO_MID + ((float)angle/90.0)*(SERVO_MID - SERVO_MIN);
	}
	else if (angle > 0 && angle <= 80)
	{
		a = ((float)angle/90.0)*(SERVO_MAX - SERVO_MID)+SERVO_MID;
	}
	return (uint32_t)a;
}

void setServo(int ang)
{
	if(ang >= -80 && ang <= 80)
	{
		angulo = ang;
	}
}

bool isMeasureReliable()
{
	if(curr_angle == SERVO_MEASURE_ANGLE && servo_ph_state == Down_Measure)
	{
		return true;
	}
	return false;
}

void runPHMeasure()
{
	servo_ph_state = Down_Wait;
	last_servo_measure = getTime();
}

void servoSpinFunction()
{
	switch (servo_ph_state) {
	case Down_Wait:
			setServo(SERVO_MEASURE_ANGLE);
			if(getTime() - last_servo_measure > SERVO_MEASURE_WAIT_TIME)
			{
				last_servo_measure = getTime();
				servo_ph_state = Down_Measure;
			}
			break;
	case Down_Measure:
		setServo(SERVO_MEASURE_ANGLE);
		if(getTime() - last_servo_measure > SERVO_MEASURE_TIME)
		{
			last_servo_measure = getTime();
			servo_ph_state = Go_To_Up_Position;
		}
		break;
	case Go_To_Up_Position:
			setServo(SERVO_UP_ANGLE);
			servo_ph_state = Idle_State;
			break;
	default:
		break;
	}
}

/**************************************************************************//**
 * @brief TIMER2_IRQHandler
 * Interrupt Service Routine TIMER2 Interrupt Line
 *****************************************************************************/
void TIMER2_IRQHandler(void)
{
	/* Clear flag for TIMER2 overflow interrupt */
	TIMER_IntClear(TIMER2, TIMER_IF_OF);

	if (counter >= COUNTER_TOP) {
		counter = 0;

		if(curr_angle > angulo)
		{
			curr_angle--;
		}
		else if (curr_angle < angulo)
		{
			curr_angle++;
		}
		TIMER_CompareBufSet(TIMER2, 1, (int)map_servo(curr_angle));
	}
	counter++;
}
