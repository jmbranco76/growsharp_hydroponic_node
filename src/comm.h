/*
 * comm.h
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#ifndef COMM_H_
#define COMM_H_

#include "stuff.h"
#include "datastructures.h"

#define BUFFER_SIZE		50
#define SI7021_ADDR	 	0x80

void parseCmd();
Control_Node *getControlMessagePtr();
void newControlMessage();
void initComm(I2C_TypeDef *i2c_d);
uint8_t readNewMessages();
uint8_t sendMessages();

#endif /* COMM_H_ */
