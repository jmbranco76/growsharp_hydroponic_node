/*
 * board_io.c
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#include "board_io.h"


#define TOP		175
#define TOPFREQ	50

volatile uint32_t compareValue;
uint32_t white_led, cena=0;

uint8_t low_level_sensor = 0;
uint8_t high_level_sensor = 0;

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB11)
 *        Switches between analog and digital clock modes.
 *****************************************************************************/
void WATER_LOW_LEVEL_PIN_irq( uint8_t pin )
{
	(void)pin;
	//low_level_sensor = GPIO_PinInGet(WATER_LOW_LEVEL_PORT, WATER_LOW_LEVEL_PIN);
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PC7)
 *        Switches between analog and digital clock modes.
 *****************************************************************************/
void WATER_HIGH_LEVEL_PIN_irq( uint8_t pin )
{
	(void)pin;
	//high_level_sensor = GPIO_PinInGet(WATER_HIGH_LEVEL_PORT, WATER_HIGH_LEVEL_PIN);
}

void initIO(void)
{
	GPIO_PinModeSet(SENSOR_ENABLE_PORT, SENSOR_ENABLE_PIN, gpioModePushPull, 1);
	//	GPIO_PinModeSet(NUTRIENT1_VALVE_PORT, NUTRIENT1_VALVE_PIN, gpioModePushPull, 1);
	//	GPIO_PinModeSet(NUTRIENT2_VALVE_PORT, NUTRIENT2_VALVE_PIN, gpioModePushPull, 1);
	//	GPIO_PinModeSet(NUTRIENT3_VALVE_PORT, NUTRIENT3_VALVE_PIN, gpioModePushPull, 1);
	//	GPIO_PinModeSet(MAIN_PUMP_PORT, MAIN_PUMP_PIN, gpioModePushPull, 1);
	//	GPIO_PinModeSet(AIR_PUMP_PORT, AIR_PUMP_PIN, gpioModePushPull, 1);
	//	GPIO_PinModeSet(PERISTALTIC_PUMP_PORT, PERISTALTIC_PUMP_PIN, gpioModePushPull, 1);
	//
	//	deployValve(0);
	//	nutrientValve(0,1);
	//	nutrientValve(0,2);
	//	nutrientValve(0,3);
	//
	//	GPIO_PinModeSet(WATER_LOW_LEVEL_PORT, WATER_LOW_LEVEL_PIN, gpioModeInputPullFilter, 1);
	//	GPIO_IntConfig(WATER_LOW_LEVEL_PORT, WATER_LOW_LEVEL_PIN, true, true, true);
	//	GPIOINT_CallbackRegister( WATER_LOW_LEVEL_PIN, WATER_LOW_LEVEL_PIN_irq );
	//
	//	GPIO_PinModeSet(WATER_HIGH_LEVEL_PORT, WATER_HIGH_LEVEL_PIN, gpioModeInputPullFilter, 1);
	//	GPIO_IntConfig(WATER_HIGH_LEVEL_PORT, WATER_HIGH_LEVEL_PIN, true, true, true);
	//	GPIOINT_CallbackRegister( WATER_HIGH_LEVEL_PIN, WATER_HIGH_LEVEL_PIN_irq );


	GPIO_PinModeSet(SERVO_PORT, SERVO_PIN, gpioModePushPull, 1);

	initPWMs();

	initServo();
}

void deployValve(uint8_t state)
{

}

void nutrientValve(uint8_t state, uint8_t id)
{

}

uint8_t getHighLevelSensor()
{
}

uint8_t getLowLevelSensor()
{
}

void initPWMs()
{
	CMU_ClockEnable(cmuClock_TIMER1, true);
	CMU_ClockEnable(cmuClock_TIMER3, true);
	compareValue = 0;
	white_led = 0;
	cena = 0;

	GPIO_PinModeSet(PWM1_PORT, PWM1_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(PWM2_PORT, PWM2_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(PWM3_PORT, PWM3_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(PWM4_PORT, PWM4_PIN, gpioModePushPull, 0);

	// Create the timer count control object initializer
	TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
	timerCCInit.mode = timerCCModePWM;
	timerCCInit.cmoa = timerOutputActionToggle;
	// Configure CC channel 0
	TIMER_InitCC(TIMER3, 0, &timerCCInit);
	TIMER_InitCC(TIMER3, 1, &timerCCInit);
	TIMER_InitCC(TIMER3, 2, &timerCCInit);

	TIMER3->ROUTE |= (TIMER_ROUTE_CC0PEN | TIMER_ROUTE_LOCATION_LOC1);
	TIMER3->ROUTE |= (TIMER_ROUTE_CC1PEN | TIMER_ROUTE_LOCATION_LOC1);
	TIMER3->ROUTE |= (TIMER_ROUTE_CC2PEN | TIMER_ROUTE_LOCATION_LOC1);

	// Set Top Value
	TIMER_TopSet(TIMER3, 8750);		// 200 Hz
	// Set the PWM duty cycle here!
	TIMER_CompareBufSet(TIMER3, 0, 0);
	TIMER_CompareBufSet(TIMER3, 1, 0);
	TIMER_CompareBufSet(TIMER3, 2, 0);
	// Create a timerInit object, based on the API default
	TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
	timerInit.prescale = timerPrescale8;
	TIMER_Init(TIMER3, &timerInit);


	TIMER_TopSet(TIMER1,TOP);
	TIMER_IntEnable(TIMER1, TIMER_IF_OF);

	/* Enable TIMER0 interrupt vector in NVIC */
	NVIC_EnableIRQ(TIMER1_IRQn);

	/* Configure timer */
	TIMER_Init(TIMER1, &timerInit);


//	setPWMs(1,128);
//	setPWMs(2,128);
//	setPWMs(3,128);
//	setPWMs(4,128);
//	while(1);

}

uint8_t setPWMs(uint8_t index, uint32_t value)
{
	uint32_t pwm;
	uint8_t ret = 0;
	switch (index) {
	case 1:
	case 2:
	case 3:
		pwm = value*35;

		if(pwm >= 8750)
		{
			pwm = 8750;
			ret = 1;
		}
		TIMER_CompareBufSet(TIMER3, index-1, pwm);
		break;
	case 4:
		pwm = value/5.1;
		if(pwm >= 50)
		{
			pwm = 50;
			ret = 1;
		}
		white_led = pwm;
		break;
	default:
		break;
	}
	return ret;
}

/**************************************************************************//**
 * @brief TIMER0_IRQHandler
 * Interrupt Service Routine TIMER0 Interrupt Line
 *****************************************************************************/
void TIMER1_IRQHandler(void)
{
	/* Clear flag for TIMER0 overflow interrupt */
	TIMER_IntClear(TIMER1, TIMER_IF_OF);
	compareValue++;


	if (compareValue >= TOPFREQ) {
		compareValue = 0;
	}

	if(compareValue < white_led)
	{
		GPIO_PinOutSet(PWM4_PORT, PWM4_PIN);
	}
	else
	{
		GPIO_PinOutClear(PWM4_PORT, PWM4_PIN);
	}

}
