/*
 * stuff.h
 *
 *  Created on: 7 de Out de 2015
 *      Author: Jos�
 */

#ifndef STUFF_H_
#define STUFF_H_

#include <stdio.h>
#include "../serial_config.h"
#include "em_device.h"
#include "em_chip.h"
#include "retargetserial.h"
#include "stddef.h"
#include "em_system.h"
#include "em_cmu.h"
#include "em_dma.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "i2cspm.h"
#include "si7013.h"
#include "rtcdriver.h"
#include "em_adc.h"
#include "dmactrl.h"
#include "string.h"
#include "comm.h"
#include "stdlib.h"
#include "string.h"
#include "time_board.h"
#include "board_io.h"
#include "i2cspm.h"
#include "spidrv.h"
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "gpiointerrupt.h"
#include "rtcdriver.h"
#include "bspconfig.h"
#include "radio.h"
#include "sensors.h"
#include "servo.h"
#include "growing_lights.h"

#ifdef __cplusplus
extern "C"
{
#endif
#include "aes.h"
#include "ti_aes128.h"
#include "si7013.h"
#include "em_timer.h"
#include "eeprom.h"
#include "json.h"
#include "ezradio_cmd.h"
#include "em_int.h"
#include "ezradio_api_lib.h"
#include "ezradio_plugin_manager.h"
#ifdef __cplusplus
}
#endif

#endif /* STUFF_H_ */
