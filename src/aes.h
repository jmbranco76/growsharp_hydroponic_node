/*
 * aes.h
 *
 *  Created on: 28 de Out de 2015
 *      Author: JMBM
 */

#ifndef AES_H_
#define AES_H_

#include "stuff.h"

uint8_t encryptData(uint8_t *data, uint8_t size);
uint8_t decryptData(uint8_t *data, uint8_t size);

#endif /* AES_H_ */
