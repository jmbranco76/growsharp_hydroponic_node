/*
 * sensors.cpp
 *
 *  Created on: 25 de Out de 2015
 *      Author: Jos�
 */

#ifndef SENSORS_CPP_
#define SENSORS_CPP_

#include "sensors.h"

uint32_t adcs[NUM_SAMPLES];

SYSTEM_ChipRevision_TypeDef chipRev;
int                         errataShift = 0;
uint32_t last_sensors_time = 0, last_sensors_time_update = 500;
static volatile bool adcConversionComplete = false;

const int8_t temp_lut[] = {-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50};
const uint16_t adc_lut[] = {812,844,877,911,945,981,1018,1055,1094,1133,1172,1213,1254,1295,1337,1380,1423,1467,1510,1554,1599,1643,1688,1733,1778,1823,1869,1913,1959,2003,2048,2092,2137,
		2181,2224,2267,2310,2353,2394,2436,2477,2517,2557,2596,2634,2672,2710,2746,2782,2817,2852,2886,2919,2951,2983,3014};

void sensorsSpinFunction()
{
	uint32_t now = getTime();
	if (now - last_sensors_time > last_sensors_time_update)
	{
		last_sensors_time = getTime();

		readSensors();


	}



}


/**************************************************************************//**
 * @brief This function is called whenever we want to measure the supply v.
 *        It is reponsible for starting the ADC and reading the result.
 *****************************************************************************/
uint32_t readSensors(void)
{
	//	DMA_Init_TypeDef       dmaInit;
	//	DMA_CfgDescr_TypeDef   descrCfg;
	//	DMA_CfgChannel_TypeDef chnlCfg;
	//	chnlCfg.highPri   = true;
	//	chnlCfg.enableInt = false;
	//	chnlCfg.select    = DMAREQ_ADC0_SCAN;
	//	chnlCfg.cb        = NULL;
	//	DMA_CfgChannel(DMA_CHANNEL, &chnlCfg);
	//
	//	descrCfg.dstInc  = dmaDataInc4;
	//	descrCfg.srcInc  = dmaDataIncNone;
	//	descrCfg.size    = dmaDataSize4;
	//	descrCfg.arbRate = dmaArbitrate1;
	//	descrCfg.hprot   = 0;
	//	DMA_CfgDescr(DMA_CHANNEL, true, &descrCfg);

	INT_Disable();
	DMA_ActivateBasic(DMA_CHANNEL,
			true,
			false,
			adcs,
			(void *)((uint32_t) &(ADC0->SCANDATA)),
			NUM_SAMPLES - 1);

	/* Start Scan */
	ADC_Start(ADC0, adcStartScan);
	while (ADC0->STATUS & ADC_STATUS_SCANACT);
	adcConversionComplete = true;
	INT_Enable();

	return NUM_SAMPLES;
}

/***************************************************************************//**
 * @brief
 *   Configure DMA usage for this application.
 *******************************************************************************/
void DMAConfig(void)
{

	DMA_Init_TypeDef       dmaInit;
	DMA_CfgDescr_TypeDef   descrCfg;
	DMA_CfgChannel_TypeDef chnlCfg;

	/* Configure general DMA issues */
	dmaInit.hprot        = 0;
	dmaInit.controlBlock = dmaControlBlock;
	DMA_Init(&dmaInit);

	/* Configure DMA channel used */
	chnlCfg.highPri   = false;
	chnlCfg.enableInt = false;
	chnlCfg.select    = DMAREQ_ADC0_SCAN;
	chnlCfg.cb        = NULL;
	DMA_CfgChannel(DMA_CHANNEL, &chnlCfg);

	descrCfg.dstInc  = dmaDataInc4;
	descrCfg.srcInc  = dmaDataIncNone;
	descrCfg.size    = dmaDataSize4;
	descrCfg.arbRate = dmaArbitrate1;
	descrCfg.hprot   = 0;
	DMA_CfgDescr(DMA_CHANNEL, true, &descrCfg);

	DMA_ActivateBasic(DMA_CHANNEL,
			true,
			false,
			adcs,
			(void *)((uint32_t) &(ADC0->SCANDATA)),
			NUM_SAMPLES - 1);
}

uint32_t *getADCsPtr()
{
	return adcs;
}

float getTemp(uint32_t adc)
{

	uint8_t i, temp_index = 255;
	for (i = 0; i < sizeof(adc_lut)/sizeof(*adc_lut); ++i)
	{
		if(adc_lut[i] < (adc*1) && adc_lut[i+1] >= (adc*1))
		{
			temp_index = i;
			break;
		}
	}
	float slope = (float)(temp_lut[temp_index+1]-temp_lut[temp_index]) / (float)(adc_lut[temp_index+1] - adc_lut[temp_index]);
	float value = slope * (float)(adc*1.0 - adc_lut[temp_index]) + (float)temp_lut[temp_index];
	return value;
}


/**************************************************************************//**
 * @brief ADC Interrupt handler (ADC0)
 *****************************************************************************/
void ADC0_IRQHandler(void)
{
	uint32_t flags;

	/* Clear interrupt flags */
	flags = ADC_IntGet( ADC0 );
	ADC_IntClear( ADC0, flags );

}

#endif /* SENSORS_CPP_ */
