/*
 * servo.h
 *
 *  Created on: 29 de Out de 2015
 *      Author: JMBM
 */

#ifndef SERVO_H_
#define SERVO_H_

#include "stuff.h"
#include "datastructures.h"

#define SERVO_PORT				gpioPortA
#define SERVO_PIN				13

typedef enum
{
	Idle_State = 0,
	Down_Wait,
	Down_Measure,
	Go_To_Up_Position
}PH_Servo_State;

void initServo(void);
void setServo(int time);
bool isMeasureReliable();
void runPHMeasure();
void servoSpinFunction();

#endif /* SERVO_H_ */
