/*
 * comm.cpp
 *
 *  Created on: 6 de Out de 2015
 *      Author: Jos�
 */

#include "comm.h"

uint8_t parse_buffer[BUFFER_SIZE + 2];
uint8_t i_ptr = 0;
bool new_message = false;
Control_Node control_message;
static uint32_t streaming_time = 1000;
static uint32_t last_streaming_time = 0, last_streaming_pv_time = 500;
I2C_TypeDef *i2c;
uint32_t last_message = 0;
uint32_t last_sensors = 0;
uint32_t last_send_control = 0;
Photovoltaic_type pv_msg;

typedef enum
{
	NutrientID = 0,
	Quantity,
	DeployValve,
	NutrientValve,
	MainPump
}RaspiKeys_type;

typedef enum
{
	SetControlMode = 0,
	PutNutrient,
	ControlValve,
	ControlPump
}RaspiAction_type;

void parseCmd() {

	int value = 0, value2 = 0, value3;
	int c;

	while ((c = RETARGET_ReadChar()) != -1 && i_ptr < BUFFER_SIZE) {
		parse_buffer[i_ptr++] = (uint8_t) c;
		parse_buffer[i_ptr] = 0;
		char * endstr = NULL;
		char * begstr = NULL;
		endstr = strstr((char *) parse_buffer, "\r\n");
		if (endstr != NULL) {
			last_message = getTime();
			i_ptr = 0;
			char *pch = strstr((char *) parse_buffer, "=");
			char *pch2;
			if (pch != NULL) {
				if (strstr((char *) parse_buffer, "PV") !=NULL) {
					value	 = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					pch2 = strstr((char *) pch + 1, ",");
					value3 = atoi(pch2 + 1);

					pv_msg.pv_v = value;
					pv_msg.pv_i = value2;
					pv_msg.pv_p = value3;

					printf("PV-V=%dmV, A=%d, P=%d\n", value, value2,value3);

				}
				else if (strstr((char *) parse_buffer, "BAT") !=NULL) {
					value	 = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");
					value2 = atoi(pch + 1);
					pch2 = strstr((char *) pch + 1, ",");
					value3 = atoi(pch2 + 1);

					pv_msg.bat_v = value;
					pv_msg.bat_i = value2;
					pv_msg.bat_p = value3;

					printf("Bat-V=%d, A=%d, P=%d\n", value, value2,value3);

				}
				else if (strstr((char *) parse_buffer, "Servo") !=NULL) {
					value	 = atoi(pch + 1);
					pch = strstr((char *) parse_buffer, ",");

					printf("Setting servo angle = %d\n", value);
					setServo(value);
				}
			}
			else
			{
				if (strstr((char *) parse_buffer, "PH") !=NULL)
				{
					runPHMeasure();

					printf("Cycle pH\n");

				}
			}
		}
	}
}

void initComm(I2C_TypeDef *i2c_d)
{
	i2c = i2c_d;
}

uint8_t readNewMessages()
{
	if(new_message)
	{
		new_message = false;
		if(control_message.force_pwm[0])
		{
			setPWMs(1,255-control_message.pwm1[0]);
		}
		if(control_message.force_pwm[1])
		{
			setPWMs(2,255-control_message.pwm2[0]);
		}
		if(control_message.force_pwm[2])
		{
			setPWMs(3,255-control_message.pwm3[0]);
		}
		if(control_message.force_pwm[3])
		{
			setPWMs(4,255-control_message.pwm4[0]);
		}
		streaming_time = control_message.streaming_time;
		last_streaming_pv_time = streaming_time/2;
		//setUnixTimestamp(control_message.timestamp);


		return 0;
	}
	return 1;
}

Control_Node *getControlMessagePtr()
{
	return &control_message;
}

void newControlMessage()
{
	new_message = true;
}

uint8_t sendMessages()
{
	uint32_t nowsm = getTime();
	if(streaming_time)
	{
		if (nowsm - last_streaming_time > streaming_time)
		{
			last_streaming_time = getTime();
			int32_t tData;
			uint32_t rhData;
			Si7013_MeasureRHAndTemp(i2c, SI7021_ADDR, &rhData, &tData);

			HydroponicNode_type hydro;
			uint32_t *adcs = getADCsPtr();
			hydro.ec = adcs[ADC_EC_CH];		// falta
			hydro.ph = adcs[ADC_PH_CH];		// falta
			hydro.rh = (int16_t)(rhData/10.0);
			hydro.temp[0] = getTemp(*(adcs+ADC_NTC1_CH))*100.0;
			hydro.temp[1] = getTemp(*(adcs+2))*100.0;
			hydro.temp[2] = getTemp(adcs[ADC_NTC3_CH])*100.0;
			hydro.temp[3] = getTemp(adcs[ADC_NTC4_CH])*100.0;
			hydro.temp[4] = (int16_t)(tData/10.0);
			hydro.temp[5] = 0;
			hydro.temp[6] = 0;
			hydro.temp[7] = 0;

			//			HydroponicNode_type hydro;
			//			hydro.ec = 0;
			//			hydro.ph = 1;
			//			hydro.rh = 2;
			//			hydro.temp[0] = 3;
			//			hydro.temp[1] = 4;
			//			hydro.temp[2] = 5;
			//			hydro.temp[3] = 6;
			//			hydro.temp[4] = 7;
			//			hydro.temp[5] = 8;
			//			hydro.temp[6] = 9;
			//			hydro.temp[7] = 10;

			sendNutrientMessage(&hydro);
		}
		if(nowsm - last_streaming_pv_time > streaming_time)
		{
			last_streaming_pv_time = getTime();

			sendPhotovoltaicMessage(&pv_msg);
		}
	}
}
