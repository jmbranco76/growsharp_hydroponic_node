/*
 * serial_config.h
 *
 *  Created on: 1 de Nov de 2015
 *      Author: Jos�
 */

#ifndef SERIAL_CONFIG_H_
#define SERIAL_CONFIG_H_

#define RETARGET_IRQ_NAME    USART2_RX_IRQHandler         /* USART IRQ Handler */
#define RETARGET_CLK         cmuClock_USART2              /* HFPER Clock */
#define RETARGET_IRQn        USART2_RX_IRQn               /* IRQ number */
#define RETARGET_UART        USART2                       /* UART instance */
#define RETARGET_TX          USART_Tx                     /* Set TX to USART_Tx */
#define RETARGET_RX          USART_Rx                     /* Set RX to USART_Rx */
#define RETARGET_LOCATION    USART_ROUTE_LOCATION_LOC1    /* Location of of the USART I/O pins */
#define RETARGET_TXPORT      gpioPortB                    /* USART transmission port */
#define RETARGET_TXPIN       3                            /* USART transmission pin */
#define RETARGET_RXPORT      gpioPortB                    /* USART reception port */
#define RETARGET_RXPIN       4                            /* USART reception pin */
#define RETARGET_USART       2                            /* Includes em_usart.h */
//#define RETARGET_PERIPHERAL_ENABLE()   \
//		GPIO_PinModeSet(BSP_BCC_ENABLE_PORT, \
//				BSP_BCC_ENABLE_PIN,  \
//				gpioModePushPull,    \
//				1);


#endif /* SERIAL_CONFIG_H_ */
